#!/bin/sh -e
target="${1:-build}"
config="${2:-config.lua}"
. ./configure.sh "$config"
make "$target"
